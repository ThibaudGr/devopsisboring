
# Usage
We use home as referential and create certs folder to store certificates
```
cd
mkdir certs
```

Create docker network
```
docker network create reverse-proxy
```

Start both the reverse proxy and the ssl handler

(need to be in the same folder as the docker-compose file)

```
docker-compose up -d
```

# New proxied container

## 1. DNS
You have to setup an A dns record (server ip pointing) for the subdomain a.example.com in the example

## 2. Start a proxied container
### With docker-compose
``` docker-compose
version: '2'
services:
  site-a:
    container_name: site-a
    image: httpd
    environment:
      - VIRTUAL_HOST=a.example.com
      - LETSENCRYPT_HOST=a.example.com
      - LETSENCRYPT_EMAIL=webmaster@example.com
    networks:
      - reverse-proxy

networks:
  reverse-proxy:
    external:
      name: reverse-proxy
```
### With docker run
```
docker run -d \
    --name site-a \
    --net reverse-proxy \
    -e 'LETSENCRYPT_EMAIL=webmaster@example.com' \
    -e 'LETSENCRYPT_HOST=a.example.com' \
    -e 'VIRTUAL_HOST=a.example.com' httpd
```


# Sources
This infos comes from the following GoogleCloud link 
https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker

I did only transpose this (nginx reverse proxy)
``` bash
docker run -d -p 80:80 -p 443:443 \
    --name nginx-proxy \
    --net reverse-proxy \
    -v $HOME/certs:/etc/nginx/certs:ro \
    -v /etc/nginx/vhost.d \
    -v /usr/share/nginx/html \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy=true \
    jwilder/nginx-proxy
```
and (ssl automatic handler)
``` bash
docker run -d \
    --name nginx-letsencrypt \
    --net reverse-proxy \
    --volumes-from nginx-proxy \
    -v $HOME/certs:/etc/nginx/certs:rw \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    jrcs/letsencrypt-nginx-proxy-companion
```
into a docker-compose file